# Cadenaz

I'm making this for fun and exercise. I want to learn _Python3_ (I'm coming from
_Python27_ and had to go back to _Python26_, but never forward) and _Qt5_ (I've
been using it a bit for work), test the code under _Linux_ and _Windows_, and I
want to track all the development with documented issues.


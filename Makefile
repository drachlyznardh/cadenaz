
all: source

run:
	@PYTHONPATH=. python3 -m cadenaz -style fusion

vrun:
	@python3 -m cadenaz -style fusion

source:
	@python3 setup.py sdist

install:
	@pip3 install dist/cadenaz-$(shell cat VERSION).tar.gz

binary:
	@python3 setup.py bdist

clean:
	@$(RM) -rf dist/ build/
	@find . -name '__pycache__' -type d | xargs $(RM) -rf
	@find . -name '*.egg-info' -type d | xargs $(RM) -rf

.PHONY: run source binary


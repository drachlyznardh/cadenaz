#!/usr/bin/python3
# encoding: utf-8

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow, QDesktopWidget
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout
from PyQt5.QtWidgets import QWidget, QLabel, QAction, QPushButton, QLineEdit

from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import Qt, QRegExp

class Store(QWidget):
	def __init__(self, sayCallback, width, height):
		super(QWidget, self).__init__()
		self._value = 0
		self._say = sayCallback
		self._initUI(width, height)

	def _initUI(self, width, height):

		l = QGridLayout()
		self.setLayout(l)

		for x in range(width):
			for y in range(height):
				e = QLineEdit('0x{:1d}{:1d}'.format(x, y), self)
				e.setValidator(QRegExpValidator(QRegExp('0x[a-fA-F0-9][a-fA-F0-9]|0b[01][01][01][01][01][01][01][01]'), self))
				l.addWidget(e, 1 + x, 1 + y)

if __name__ == '__main__':

	import sys
	from PyQt5.QtWidgets import QApplication

	print('Hello world')

	app = QApplication(sys.argv)
	frame = Store()

	exit_value = app.exec_()
	print('Goodbye, cruel world')
	sys.exit(exit_value)


#!/usr/bin/python3
# encoding: utf-8

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow, QDesktopWidget
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QWidget, QLabel, QAction, QPushButton

from PyQt5.QtCore import Qt

from .store import Store

class IncreaseDecreasePanel(QWidget):
	def __init__(self, sayCallback):
		super(QWidget, self).__init__()
		self._value = 0
		self._say = sayCallback
		self._initUI()

	def _initUI(self):

		l = QVBoxLayout()
		self.setLayout(l)

		self._vLabel = QLabel('%s' % self._value)
		self._vLabel.setAlignment(Qt.AlignCenter)
		l.addWidget(self._vLabel)

		i = QPushButton('+', self)
		i.clicked.connect(self._increase)
		l.addWidget(i)

		d = QPushButton('-', self)
		d.clicked.connect(self._decrease)
		l.addWidget(d)

	def _increase(self):
		self._value += 1
		self._value %= 256
		self._vLabel.setText('%s' % self._value)
		self._say('Incremented by 1', 500)

	def _decrease(self):
		self._value -= 1
		self._value %= 256
		self._vLabel.setText('%s' % self._value)
		self._say('Decremented by 1', 500)

class MainPanel(QWidget):
	def __init__(self, sayCallback):
		super(QWidget, self).__init__()
		self._value = 0
		self._say = sayCallback
		self._initUI()

	def _initUI(self):

		lv = QVBoxLayout()
		lh = QHBoxLayout()
		self.setLayout(lv)

		lv.addStretch(1)
		lv.addLayout(lh)
		lv.addStretch(1)

		lh.addStretch(1)
		lh.addWidget(Store(self._say, 8, 8))
		lh.addStretch(1)

class MainWindow(QMainWindow):
	def __init__(self):
		super(MainWindow, self).__init__()
		self._initUI()

	def _initUI(self):

		menu = self.menuBar()
		fileMenu = menu.addMenu('&File')
		quitAction = QAction('&Quit', self)
		quitAction.triggered.connect(QApplication.instance().quit)
		fileMenu.addAction(quitAction)

		self.setCentralWidget(MainPanel(self.say))
		self.adjustSize()
		self._center()

		self.setWindowTitle('Cadenaz')
		self.say('Ready', 500)
		self.show()

	def _center(self):
		g = self.frameGeometry()
		d = QDesktopWidget().availableGeometry().center()
		g.moveCenter(d)
		self.move(g.topLeft())

	def say(self, message, timeout):
		self.statusBar().showMessage(message, timeout)

if __name__ == '__main__':

	import sys
	from PyQt5.QtWidgets import QApplication

	print('Hello world')

	app = QApplication(sys.argv)
	frame = MainWindow()

	exit_value = app.exec_()
	print('Goodbye, cruel world')
	sys.exit(exit_value)


#!/usr/bin/python3
# encoding: utf-8

import sys
from PyQt5.QtWidgets import QApplication

from .gui.mainwindow import MainWindow

if __name__ == '__main__':
	print('Hello world')

	app = QApplication(sys.argv)
	frame = MainWindow()

	exit_value = app.exec_()
	print('Goodbye, cruel world')
	sys.exit(exit_value)


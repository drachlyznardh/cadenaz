#!/usr/bin/python3
# encoding: utf-8

with open('VERSION', 'r') as ofd:
    version = ofd.read().strip()

with open('README.md', 'r') as ofd:
    readme = ofd.read()

from setuptools import setup, find_packages
setup(
    name='cadenaz',
    version=version,
    url='https://gitlab.com/drachlyznardh/cadenaz',
    author='Ivan Simonini',
    author_email='ivan.simonini@roundhousecode.com',
    project_urls={
        'Source':'https://gitlab.com/drachlyznardh/cadenaz',
        'Tracker':'https://gitlab.com/drachlyznardh/cadenaz/issues'
    },
    description='A Cadenaz emulator',
    long_description=readme,
    long_description_content_type='text/markdown',
    install_requires=['PyQt5==5.10'],
    scripts=['bin/cadenaz'],
    package_dir={ '': 'src' },
    packages=find_packages(where='src'),
    include_package_data=True
)

